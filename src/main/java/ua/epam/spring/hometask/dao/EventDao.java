package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.Event;

import java.util.Collection;

public interface EventDao {
    Event getEventByName (String name);

    Event saveEvent(Event object);

    void removeEvent(Event object);

    Event getEventById(Long id);

    Collection<Event> getAllEvents();
}
