package ua.epam.spring.hometask.dao;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import ua.epam.spring.hometask.domain.Event;

import java.util.Collection;
import java.util.Map;

public class EventDaoImpl implements EventDao, ApplicationContextAware {
    private Map<Long, Event> eventMap;
    private ApplicationContext ctx;

    public EventDaoImpl(Map<Long, Event> eventMap) {
        this.eventMap = eventMap;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }

    @Override
    public Event getEventByName(String name) {
        Event event;
        for (long key : eventMap.keySet()) {
            event = eventMap.get(key);
            if (name.equals(event.getName())) {
                return event;
            }
        }
        return null;
    }

    @Override
    public Event saveEvent(Event object) {
        long id = checkForExistence(object);
        if (id != 0) {
            object.setId(id);
            eventMap.replace(id, object);
            return object;
        }
        long placeInMap = getMaxKey();
        object.setId(placeInMap);
        eventMap.put(placeInMap, object);
        return object;
    }

    private long checkForExistence(Event object) {
        Event event;
        for (long key : eventMap.keySet()) {
            event = eventMap.get(key);
            if (event.equals(object)) {
                return event.getId();
            }
        }
        return 0;
    }

    private long getMaxKey() {
        long maxKey = 0;
        for (long key : eventMap.keySet()) {
            if(key > maxKey) {
                maxKey = key;
            }
        }
        return maxKey;
    }

    @Override
    public void removeEvent(Event object) {
        Event event;
        for (long key : eventMap.keySet()) {
            event = eventMap.get(key);
            if (event.equals(object)) {
                eventMap.remove(key, event);
                return;
            }
        }
    }

    @Override
    public Event getEventById(Long id) {
        return eventMap.get(id);
    }

    @Override
    public Collection<Event> getAllEvents() {
        return eventMap.values();
    }
}
