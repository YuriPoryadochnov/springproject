package ua.epam.spring.hometask.dao;

import ua.epam.spring.hometask.domain.User;

import java.util.Collection;

public interface UserDao {
    User getUserByEmail (String email);

    User saveUser(User object);

    void removeUser(User object);

    User getUserById(Long id);

    Collection<User> getAllUsers();
}
