package ua.epam.spring.hometask.dao;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import ua.epam.spring.hometask.domain.User;

import java.util.Collection;
import java.util.Map;

public class UserDaoImpl implements UserDao, ApplicationContextAware {
    private Map<Long, User> usersMap;
    private ApplicationContext ctx;

    public UserDaoImpl(Map<Long, User> usersMap) {
        this.usersMap = usersMap;
    }

    public User getUserByEmail(String email) {
        User user;
        for (long key : usersMap.keySet()) {
            user = usersMap.get(key);
            if (email.equals(user.getEmail())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public User saveUser(User object) {
        long id = checkForExistence(object);
        if (id != 0) {
            object.setId(id);
            usersMap.replace(id, object);
            return object;
        }
        long placeInMap = getMaxKey();
        object.setId(placeInMap);
        usersMap.put(placeInMap, object);
        return object;
    }

    private long checkForExistence(User object) {
        User user;
        for (long key : usersMap.keySet()) {
            user = usersMap.get(key);
            if (user.equals(object)) {
                return user.getId();
            }
        }
        return 0;
    }

    private long getMaxKey() {
        long maxKey = 0;
        for (long key : usersMap.keySet()) {
            if(key > maxKey) {
                maxKey = key;
            }
        }
        return maxKey;
    }

    @Override
    public void removeUser(User object) {
        User user;
        for (long key : usersMap.keySet()) {
            user = usersMap.get(key);
            if (user.equals(object)) {
                usersMap.remove(key, user);
                return;
            }
        }
    }

    @Override
    public User getUserById(Long id) {
        return usersMap.get(id);
    }

    @Override
    public Collection<User> getAllUsers() {
        return usersMap.values();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}
