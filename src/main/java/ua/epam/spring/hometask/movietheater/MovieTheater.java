package ua.epam.spring.hometask.movietheater;

import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.movietheater.operations.AdminOperations;
import ua.epam.spring.hometask.movietheater.operations.UserOperations;

import java.util.Scanner;

public class MovieTheater {
    private AdminOperations adminOperations;
    private UserOperations userOperations;
    private Scanner scanner;
    private User user;

    public MovieTheater(AdminOperations adminOperations, UserOperations userOperations, User user) {
        this.adminOperations = adminOperations;
        this.userOperations = userOperations;
        this.user = user;
        this.scanner = new Scanner(System.in);
        adminOperations.setScanner(scanner);
        userOperations.setScanner(scanner);
    }

    public void launchUserInterface() {
        String userInput;
        while (true) {
            System.out.println("Пользовательские команды:\n" +
                    "1. Войти в систему\n" +
                    "2. Зарегистрироваться и войти\n" +
                    "3. Вывести события\n" +
                    "4. Вывести стоимость билетов\n" +
                    "5. Купить билеты\n" +
                    "6. Перейти к командам администратора\n" +
                    "7. Выйти");
            userInput = scanner.nextLine();
            switch (userInput) {
                case "1":
                    userOperations.loginUser();
                    break;
                case "2":
                    userOperations.registerUser();
                    break;
                case "3":
                    userOperations.getEvents();
                    break;
                case "4":
                    userOperations.getPriceOfTickets();
                    break;
                case "5":
                    userOperations.buyTickets();
                    break;
                case "6":
                    launchAdminInterface();
                    break;
                case "7":
                    return;
            }
        }
    }

    private void launchAdminInterface() {
        if (null == user.getFirstName() || "User".equals(user.getRole())) {
            System.out.println("Вы не являетесь администратором");
            return;
        }
        String adminInput;
        while (true) {
            System.out.println("1. Ввести событие\n" +
                    "2. Вывести купленные билеты\n" +
                    "3. Выйти из режима администратора");
            adminInput = scanner.nextLine();
            switch (adminInput) {
                case "1":
                    adminOperations.addNewEvent();
                    break;
                case "2":
                    adminOperations.getPurchasedTickets();
                    break;
                case "4":
                    adminOperations.getAllEvent();
                    break;
                case "3":
                    return;
            }
        }
    }
}
