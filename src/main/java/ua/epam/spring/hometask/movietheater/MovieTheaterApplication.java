package ua.epam.spring.hometask.movietheater;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Arrays;

public class MovieTheaterApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = new FileSystemXmlApplicationContext("src/main/resources/spring.xml");
        MovieTheater movieTheater = (MovieTheater) ctx.getBean("movieTheater");
        movieTheater.launchUserInterface();
    }
}
