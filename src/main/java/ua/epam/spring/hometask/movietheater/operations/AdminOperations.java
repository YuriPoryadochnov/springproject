package ua.epam.spring.hometask.movietheater.operations;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import ua.epam.spring.hometask.domain.*;
import ua.epam.spring.hometask.service.AuditoriumService;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class AdminOperations implements ApplicationContextAware {
    private EventService eventService;
    private BookingService bookingService;
    private AuditoriumService auditoriumService;
    private Scanner scanner;
    private User user;
    private ApplicationContext ctx;

    public AdminOperations(EventService eventService, BookingService bookingService, AuditoriumService auditoriumService,
                           User user) {
        this.eventService = eventService;
        this.bookingService = bookingService;
        this.auditoriumService = auditoriumService;
        this.user = user;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public void addNewEvent() {
        Event event = (Event) ctx.getBean("event");
        System.out.println("Введите название события:");
        event.setName(scanner.nextLine());
        System.out.println("Введите базовую цену на билет:");
        try {
            event.setBasePrice(Double.parseDouble(scanner.nextLine()));
        } catch (NumberFormatException e) {
            System.out.println("Неверный формат данных");
            return;
        }
        System.out.println("Введите рейтинг события (1 - низкий, 2 - средний, 3 - высокий):");
        switch (scanner.nextLine()) {
            case "1":
                event.setRating(EventRating.LOW);
                break;
            case "2":
                event.setRating(EventRating.MID);
                break;
            case "3":
                event.setRating(EventRating.HIGH);
                break;
            default:
                System.out.println("Неверный формат данных");
                return;
        }
        boolean isEnoughAirDates = false;
        while (!isEnoughAirDates) {
            System.out.println("Введите дату события:");
            LocalDate localDate = LocalDate.parse(scanner.nextLine());
            System.out.println("Введите время события:");
            LocalTime localTime = LocalTime.parse(scanner.nextLine());
            LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);

            boolean isEnoughAuditoriums = false;
            while (!isEnoughAuditoriums) {
                System.out.println("Введите название аудитории:");
                Auditorium auditoriumType = auditoriumService.getByName(scanner.nextLine());
                if (null == auditoriumType) {
                    System.out.println("Такой аудитории нет");
                    continue;
                }
                Auditorium auditorium = (Auditorium) ctx.getBean("auditorium");
                auditorium.setAuditorium(auditoriumType);
                event.addAirDateTime(localDateTime, auditorium);
                System.out.println("Введите Yes, если хотите ввести одну аудиторию:");
                if (!"Yes".equals(scanner.nextLine())) {
                    isEnoughAuditoriums = true;
                }
            }
            System.out.println("Введите Yes, если хотите ввести ещё одно событие на данную аудиторию:");
            if (!"Yes".equals(scanner.nextLine())) {
                isEnoughAirDates = true;
            }
        }
        eventService.save(event);
    }

    public void getPurchasedTickets() {
        System.out.println("Введите название события:");
        Event event = eventService.getByName(scanner.nextLine());
        if (null == event) {
            System.out.println("Такого события не существует");
            return;
        }
        System.out.println("Введите дату события:");
        LocalDate localDate = LocalDate.parse(scanner.nextLine());
        System.out.println("Введите время события:");
        LocalTime localTime = LocalTime.parse(scanner.nextLine());
        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        Set<Ticket> ticketSet = bookingService.getPurchasedTicketsForEvent(event, localDateTime);
        for (Ticket ticket : ticketSet) {
            System.out.println(ticket.toString());
        }
    }

    public void getAllEvent(){
        List<Event> eventList = new ArrayList<Event>(eventService.getAll());
        for(Event event : eventList) {
            System.out.println(event.toString());
        }
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}
