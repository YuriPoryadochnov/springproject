package ua.epam.spring.hometask.movietheater.operations;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.Ticket;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.BookingService;
import ua.epam.spring.hometask.service.EventService;
import ua.epam.spring.hometask.service.UserService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

public class UserOperations implements ApplicationContextAware {
    private UserService userService;
    private EventService eventService;
    private BookingService bookingService;
    private Scanner scanner;
    private User userInSystem;
    private ApplicationContext ctx;

    public UserOperations(UserService userService, EventService eventService, BookingService bookingService,
                          User user) {
        this.userService = userService;
        this.eventService = eventService;
        this.bookingService = bookingService;
        this.userInSystem = user;
    }

    public void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public void registerUser() {
        User user = (User) ctx.getBean("user");
        System.out.println("Введите имя:");
        user.setFirstName(scanner.nextLine());
        System.out.println("Введите фамилию:");
        user.setLastName(scanner.nextLine());
        System.out.println("Введите дату рождения:");
        user.setDateOfBirth(LocalDateTime.of(LocalDate.parse(scanner.nextLine()), LocalTime.of(0, 0)));
        System.out.println("Введите почту:");
        user.setEmail(scanner.nextLine());
        System.out.println("Введите пароль:");
        user.setPassword(scanner.nextLine());
        user.setRole("User");
        userService.save(user);
    }

    public void loginUser() {
        System.out.println("Введите почту:");
        User user = userService.getUserByEmail(scanner.nextLine());
        System.out.println("Введите пароль:");
        if (null != user && scanner.nextLine().equals(user.getPassword())) {
            userInSystem.setUser(user);
            System.out.println("Вы вошли в систему");
            System.out.println(userInSystem.toString());
            return;
        }
        System.out.println("Такого пользователя нет или пароль не является верным");
    }

    public void getEvents() {
        List<Event> eventList = new ArrayList<Event>(eventService.getAll());
        for(Event event : eventList) {
            System.out.println(event.toString());
        }
    }

    public void getPriceOfTickets() {
        System.out.println("Название события:");
        Event event = eventService.getByName(scanner.nextLine());
        System.out.println("Введите дату события:");
        LocalDate localDate = LocalDate.parse(scanner.nextLine());
        System.out.println("Введите время события:");
        LocalTime localTime = LocalTime.parse(scanner.nextLine());
        LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
        if (null != event && event.airsOnDateTime(localDateTime)) {
            System.out.println("Введите места через запятую:");
            List<String> stringList = Arrays.asList(scanner.nextLine().split(","));
            Set<Long> seats = new TreeSet<>();
            try {
                for (String s : stringList) {
                    seats.add(Long.parseLong(s));
                }
                if(null == userInSystem.getEmail()){
                    System.out.println(bookingService.getTicketsPrice(event, localDateTime, null, seats));
                }
                else {
                    System.out.println(bookingService.getTicketsPrice(event, localDateTime, userInSystem, seats));
                }
            } catch (NumberFormatException e) {
                System.out.println("Неверный формат данных");
            }
        }
        System.out.println("Событие не существует");
    }

    public void buyTickets() {
        boolean isEnoughTickets = false;
        Set<Ticket> tickets = new TreeSet<>();
        while (!isEnoughTickets) {
            System.out.println("Название события:");
            Event event = eventService.getByName(scanner.nextLine());
            System.out.println("Введите дату события:");
            LocalDate localDate = LocalDate.parse(scanner.nextLine());
            System.out.println("Введите время события:");
            LocalTime localTime = LocalTime.parse(scanner.nextLine());
            LocalDateTime localDateTime = LocalDateTime.of(localDate, localTime);
            if (null != event && event.airsOnDateTime(localDateTime)) {
                System.out.println("Введите место:");
                try {
                    Ticket ticket = (Ticket) ctx.getBean("ticket");
                    if(null != userInSystem.getEmail()) {
                        ticket.setUser(userInSystem);
                    }
                    ticket.setDateTime(localDateTime);
                    ticket.setEvent(event);
                    ticket.setSeat(Long.parseLong(scanner.nextLine()));
                    tickets.add(ticket);
                } catch (NumberFormatException e) {
                    System.out.println("Неверный формат данных");
                    return;
                }
            }
            System.out.println("Введите Yes, если хотите ввести ещё один билет:");
            if(!"Yes".equals(scanner.nextLine())) {
                isEnoughTickets = true;
            }
        }
        bookingService.bookTickets(tickets);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}
