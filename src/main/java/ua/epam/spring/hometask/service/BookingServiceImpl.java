package ua.epam.spring.hometask.service;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import ua.epam.spring.hometask.domain.*;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class BookingServiceImpl implements BookingService, ApplicationContextAware {
    private ApplicationContext ctx;

    @Override
    public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, @Nullable User user, @Nonnull Set<Long> seats) {
        DiscountService discountService = (DiscountService) ctx.getBean("discountService");
        Auditorium auditorium = event.getAuditoriums().get(dateTime);
        long numberOfTicket = 1;
        double totalPrice = 0;
        double priceScale;
        for (Long seat : seats) {
            double seatPrice = event.getBasePrice();
            if (event.getRating() == EventRating.HIGH) {
                priceScale = 1.2;
                seatPrice *= priceScale;
            }
            if (event.getAuditoriums().get(dateTime).getVipSeats().contains(seat)){
                priceScale = 2;
                seatPrice *= priceScale;
            }
            byte discount = discountService.getDiscount(user, event, dateTime, numberOfTicket);
            totalPrice += discount == 0 ? seatPrice : seatPrice * discount / 100;
            numberOfTicket++;
        }
        return totalPrice;
    }

    @Override
    public void bookTickets(@Nonnull Set<Ticket> tickets) {
        User userInSystem = (User) ctx.getBean("userInSystem");
        for (Ticket ticket : tickets) {
            Auditorium auditorium = ticket.getEvent().getAuditoriums().get(ticket.getDateTime());
            if (null != auditorium && auditorium.getNumberOfSeats() >= ticket.getSeat() && !auditorium.isPurchasedSeat(ticket.getSeat())) {
                auditorium.addSeatToPurchasedSeats(ticket.getSeat());
                auditorium.addTicketToPurchasedTickets(ticket);
                registerTicketForUser(userInSystem, ticket);
            }
        }
    }

    private void registerTicketForUser(User user, Ticket ticket) {
        if (null != user.getEmail()) {
            user.getTickets().add(ticket);
        }
    }

    @Nonnull
    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
        Auditorium auditorium = event.getAuditoriums().get(dateTime);
        if (null == auditorium) {
            return new TreeSet<>();
        }
        return event.getAuditoriums().get(dateTime).getPurchasedTickets();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}
