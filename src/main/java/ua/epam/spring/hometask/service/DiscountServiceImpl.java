package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;
import ua.epam.spring.hometask.service.discountstrategy.DiscountStrategy;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class DiscountServiceImpl implements DiscountService {
    private ArrayList<DiscountStrategy> strategies;

    public DiscountServiceImpl(ArrayList<DiscountStrategy> strategies) {
        this.strategies = strategies;
    }

    @Override
    public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
        byte discount = 0;
        for (DiscountStrategy strategy : strategies) {
            byte newDiscount = strategy.getDiscount(user, event, airDateTime, numberOfTickets);
            discount = discount < newDiscount ? newDiscount : discount;
        }
        return discount;
    }
}
