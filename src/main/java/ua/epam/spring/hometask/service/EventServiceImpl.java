package ua.epam.spring.hometask.service;

import ua.epam.spring.hometask.dao.EventDao;
import ua.epam.spring.hometask.domain.Event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

public class EventServiceImpl implements EventService {
    private EventDao eventDao;

    public EventServiceImpl(EventDao eventDao) {
        this.eventDao = eventDao;
    }

    @Nullable
    @Override
    public Event getByName(@Nonnull String name) {
        return eventDao.getEventByName(name);
    }

    @Override
    public Event save(@Nonnull Event object) {
        return eventDao.saveEvent(object);
    }

    @Override
    public void remove(@Nonnull Event object) {
        eventDao.removeEvent(object);
    }

    @Override
    public Event getById(@Nonnull Long id) {
        return eventDao.getEventById(id);
    }

    @Nonnull
    @Override
    public Collection<Event> getAll() {
        return eventDao.getAllEvents();
    }
}
