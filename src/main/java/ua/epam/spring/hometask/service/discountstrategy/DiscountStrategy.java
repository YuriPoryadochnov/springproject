package ua.epam.spring.hometask.service.discountstrategy;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

public interface DiscountStrategy {
    byte getDiscount(User user, Event event, LocalDateTime dateTime, long numberOfTicket);
}
