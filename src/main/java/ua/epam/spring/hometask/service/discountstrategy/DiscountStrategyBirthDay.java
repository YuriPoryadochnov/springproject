package ua.epam.spring.hometask.service.discountstrategy;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.time.Duration;
import java.time.LocalDateTime;

public class DiscountStrategyBirthDay implements DiscountStrategy {
    @Override
    public byte getDiscount(User user, Event event, LocalDateTime dateTime, long numberOfTicket) {
        byte discount = 0;
        if (null == user || null == user.getDateOfBirth()) {
            return discount;
        }
        int daysFromBirthday = 5;
        LocalDateTime currentDateTime = LocalDateTime.now();
        LocalDateTime birthDateTime = dateTime.withYear(currentDateTime.getYear());
        if(Duration.between(currentDateTime, birthDateTime).toDays() <= daysFromBirthday){
            discount = 5;
        }
        return discount;
    }
}
