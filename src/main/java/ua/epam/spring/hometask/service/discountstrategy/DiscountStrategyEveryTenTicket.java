package ua.epam.spring.hometask.service.discountstrategy;

import ua.epam.spring.hometask.domain.Event;
import ua.epam.spring.hometask.domain.User;

import java.time.LocalDateTime;

public class DiscountStrategyEveryTenTicket implements DiscountStrategy {
    @Override
    public byte getDiscount(User user, Event event, LocalDateTime dateTime, long numberOfTicket) {
        byte discount = 0;
        if (numberOfTicket % 10 == 0){
            discount = 50;
        }
        return discount;
    }
}
